package pl.codementors.sż;

import javafx.animation.Transition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Zadanie1 extends Application{

    private void makeBorder(Group root, double x, double y, double xTransition,
                            double yTransition, int count) {
        Random random = new Random();
        int i;
        int radius = 25;    //promien kola
        for ( i =0; i < count; i++ ) {
            Rectangle rec = new Rectangle(50,50);
            Circle circle = new Circle(25);
            if(i%2 ==0){
                circle.setCenterX((x + i * xTransition)+radius);
                circle.setCenterY((y + i * yTransition)+radius);
                circle.setFill(Color.color(random.nextDouble(),
                    random.nextDouble(), random.nextDouble()));
                root.getChildren().add(circle);

            }else {
                rec.setX(x + i * xTransition);
                rec.setY(y + i * yTransition);
                rec.setFill(Color.color(random.nextDouble(),
                        random.nextDouble(), random.nextDouble()));
                root.getChildren().add(rec);
            }
        }
    }


    public void start(Stage stage) {

            Group root = new Group();
            //ustawienia gwiazdy
            Polygon polygon = new Polygon(300,200, 271,260, 205,269, 252,315, 241,381, 300,350, 359,381, 348,315, 395,269, 329,260 ); //punkt 00
//            polygon.setStrokeWidth(5);
            //kolor
            polygon.setFill(Color.BLUE);

            //ustawienia tekstu
            Text text = new Text("homeworks are awesome");
            //pozycja na scenie
            text.setX(215);
            text.setY(150);
            //kolor
            text.setStroke(Color.GREEN);



            //dodanie do roota sceny
            root.getChildren().add(polygon);
            root.getChildren().add(text);
            makeBorder(root, 0, 0, 50, 0, 600/50);
            makeBorder(root, 50, 600-50, 50, 0, 600/50);
            makeBorder(root, 0, 0, 0, 50, 600/50);
            makeBorder(root, 550, 50, 0, 50, 550/50);



            Scene scene = new Scene(root, 600, 600, Color.BLACK);
            stage.setScene(scene);
            stage.show();

        }

        public static void main(String[] args) {
            launch(args);
        }
}
